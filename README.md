# Ruslan Kladko Resume

## How to find me

* Email: rkladko@gmail.com
* Phone: +47 40 58 67 48
* Skype: live:.cid.dc20b26c89f71fdc
* Github: [@rooslunn](https://www.github.com/rooslunn)
* Gitlab: [@rooslunn](https://gitlab.com/rooslunn)
* Telegram: https://t.me/rooslunn

## Job Titles

* Middle/Senior PHP Developer
* System Architect
* Fullstack Developer

## Core Responsibilities

* Ongoing development
* Refactoring, profiling
* REST API design
* Technical architecture
* Communication with business stakeholders
* Transition to DevOps
* [12 Factor App](https://12factor.net/) methodology
* Tests for technical interviews
* Code Review
* Integration with web services
* Highload testing

## Core Technologies

#### [Code samples](https://gitlab.com/rooslunn/resume/-/wikis/showroom)

* Programming Languages
    * PHP, Golang
    * JavaScript
    * C, Java, Python
* Frameworks and Libraries
    * Laravel, Symfony, Yii and other popular
    * jQuery, VueJS
* Paradigms and Methodologies
    * SOLID, CUPID, DRY, KISS, YAGNI
    * TDD, BDD
    * Agile (Scrum & Kanban)
* Databases and Data Tools
    * MySQL
    * Redis
    * PostgreSQL, Oracle, SQLite
* Cloud and Infrastructure Providers
    * AWS, GCP
* Other Tools and Services:
    * Docker, Apache Solr, Apache jMeter, Sphinx

## Work Experience

### _Jan 2011 - Present_: **PHP Developer, full time remote or onsite**

* Kyiv, Odessa, Lviv, Bangkok

### Projects and Responsibilities

* Project's setup and architecture
* Backend and frontend from scratch
* Crawlers and scrapers
* Testing, Profiling and Code Review
* Refactoring, Legacy code support
* Integration with Web-services
* Site Reliability Engineering
* Highload testing
* Test preparation for technical interviews
* Working with international clients

### _Jan 2007 - Sep 2010_: **Head of VISA card processing center**

* Imexbank, Odessa

### Projects and Responsibilities

* VISA Card Processing Go Live (Issuing and Acquiring).
* Required certifications (Stripe/EMV issuing and acquiring)
* OpenWay Card Processing Software implementation
* Risk and Fraud Management, PCI DSS
* Network Infrastructure support


### _Nov 2001 - Nov 2007_: **Software/Hardware Engineer**

* Marine Transport Bank, Odessa

### Projects and Responsibilities

* Card Software support (IS-Card, DUET)
* ATM/POS hardware and software support
* RS-BANK software integrations

## Education

* _1993 - 1998_: **East Ukrainian National University**
* Specialist degree in IT-management (recognized in Germany)

## Professional development, courses

* 2023, [Java](https://stepik.org/cert/2069025)
* 2022, [Golang](https://stepik.org/cert/1800913)
* 2016, [Нейронные сети](https://stepik.org/cert/25928)
* 2014, [MongoDB for Developers](https://ti-user-certificates.s3.amazonaws.com/ae62dcd7-abdc-4e90-a570-83eccba49043/8722169f-f5ca-523c-8551-cfc9570cb160-ruslan-kladko-dec86973-ad99-5bd1-835f-230fd126ef13-certificate.pdf)


## Projects/Companies
* _Feb 2022 — Mar 2023_, Remote work for: https://www.qualityunit.com/; https://www.eramba.org/; https://www.mtc.co.uk/
    - Ongoing Development support
    - Fixing and refactoring legacy
    - DB/SQL optimizations

* _Jun 2021 — Jan 2022_, **Production Devs; Crypto, stocks, commodities online trading platform**
    - Ongoing development
    - Fixing/refactoring. Adding TDD step in the dev process.
    - Optimizing performance, fixing bottlenecks
    - Fine-tuning data exchange channels with providers via WS and API (Binance, CryptoCompare, TwelveData, Polygon).
    - Mentoring Junior and Middle Developers.
    - TechStack: Laravel, Mysql/Postgre, Redis, ReactPHP, Swoole.

* _Jan 2021 — Jun 2021_, **Miratech, Game Industry**
    - Project: Web launcher for gambling games stack (casino, slots, etc.)
    - Take decisions on system architecture and technical stack: Laravel, MySQL, Redis, VueJS, TailwindCSS
    - Define stages, tasks and other phases.
    - Implementation and go Live.
    - Ongoing development
    - Bug fixing/refactoring/implement test driven development (codeception)


* _Apr 2020 — Aug 2020_, **UnitedPrint SE, Radebeull, Germany**
    - Ongoing development
    - Bug fixing/refactoring/implement test driven development (codeception)

* _Sep 2019 — March 2020_, **Dreamscape.com (web.com)**
    - Ongoing development
    - Response time/DB optimizations
    - No frameworks, lot of legacy procedural code, so refactoring to SOLID, OOP is prerogative
    - Article Editor tool (online help editor for all apps): Laravel, MySQL, VueJS, Vanilla JS

* _Feb 2019 — May 2019_, **Spyse.com**
    - Slow queries optimisations, minimizing response time
    - CodeIgniter, PostgreSQL

* _Jul 2018 — Dec 2018_, **Pilulka.cz**
    - Implementing REST API for mobile apps
    - Ongoing development
    - Laravel, Symfony, MySQL, Elastic Search

* _Feb 2018 — Jun 2018_
    - Freelance projects
    - Skill’s improvement time - MOOC (Coursera, Stepic), AWS, GCP

* _Oct 2017 - Jan 2018_, **Academos**, Online book store
    - Unifying interface for managing suppliers files (upload, registry etc.)
    - Ongoing support, development, refactoring
    - In-house framework development, jQuery, MySQL

* _Apr 2017 - Aug 2017_, **Business VPN**
    - REST API design and implementation
    - Laravel, GraphQL, PostgreSQL, Vue.js

* _Sep 2016 - Feb 2017_, **ouffer.com**, CRM
    - Ongoing development, Refactoring
    - In-house PHP Framework, jQuery,MySQL, Sphinx

* _May 2016 - July 2016_, **Subscribeasy.com**, CRM
    - Ongoing development, Refactoring
    - Laravel, jQuery, Vue.js, MySQL

* _Jan 2016 - Feb 2016_, **Spiritum**
    - Project setup and architecture.
    - Technical interviews, hiring

* _Sep 2015 - Nov 2015_, **VKINO, vkino.com.ua**, online movie tickets
    - Migrated backend to Laravel
    - Back-end and Front-end optimisations (caching, assets management, etc.)
    - MySQL db scheme refactoring

* _Mar 2015 - Jun 2015_, **Gravity4, tr.im**, Advertising and PR Services
    - Tr.im project Ongoing development
    - Integration with other Gravity projects

* _Jul 2014 - Feb 2015_, **3DPrinterOS**
    - Ongoing development, upgrading, refactoring and adding new features
    - Integration with Stripe and i.materialize
    - PhotoBooth (self-service registration terminal for mariners),
    - VTA-Rvd (admin system for crewing agencies)

* _Jul 2013 - Jun 2014_, **SystemDoc**, Moscow City Administration
    - Ongoing development, upgrading, refactoring and adding new features

* _Feb 2013 - Jul 2013_, **GPS Control**, Cargo Monitoring
    - Web and Adnroid application for monitoring and dispatching vehicle online

* _Jul 2012 - Dec 2012_, **LIKS CARD SERVICE**, Transportation and Logistics
    - “craigslist” for cargo agents
    - UI implementation (Twitter Bootstrap)
    - REST API

* _Sep 2011 - May 2012_, **Pixonic**, Game Development**
    - Designed and developed backend for game.
    - Functional testing, code optimization for DAU 100K/day
    - Documentation (class and sequence diagrams, backend's interface API)
    - Admin Panel (users, billing, initial settings)
    - High-load testing, code optimization and server tuning

